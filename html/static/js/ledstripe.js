var ledstripe = {
  radius: 2,
  margin: 3,
  sep: 1,
  columns: 128,
  rows: 8,
  msgStack: [],
  currentFrame: 0,
  curentMessage: {},

  init: function() {
    this.width = this.columns * (this.radius * 2 + this.sep) - this.sep + 2 * this.margin;
    this.height = this.rows * (this.radius * 2 + this.sep) - this.sep + 2 * this.margin;
    var c = document.getElementById("ledStripeCanvas");
    c.width = this.width;
    c.height = this.height;
    var ctx = c.getContext("2d");

    ctx.beginPath();
    ctx.rect(0,0,this.width,this.height);
    ctx.fillStyle="#333333";
    ctx.fill();

     for (var y=0; y < this.rows; y++) {
      for (var x=0; x < this.columns; x++) {
        ctx.beginPath();
        ctx.arc(
          (this.sep + 2 * this.radius) * x + this.margin + this.radius,
          (this.sep + 2 * this.radius) * y + this.margin + this.radius,
          this.radius, 0, 2*Math.PI);
        ctx.strokeStyle="white";
        ctx.stroke();     
      }
    }
  },

  update_canvas: function(data) {
    var c = document.getElementById("ledStripeCanvas");
    var ctx = c.getContext("2d");

    for (var i = 0; i < data.length; i++) {
      var y = i % this.rows;
      var x = (i - y) / this.rows;
      if (x % 2 == 1) {
         y = this.rows - 1 - y;
      }
      ctx.beginPath();
      ctx.arc(
        (this.sep + 2 * this.radius) * x + this.margin + this.radius,
        (this.sep + 2 * this.radius) * y + this.margin + this.radius,
        this.radius, 0, 2*Math.PI);
      var rgb = data[i];
      ctx.fillStyle = '#' + rgb;
      ctx.fill();
    }
  },

};

function start_stripe(columns, rows) {
  ledstripe.columns = columns;
  ledstripe.rows = rows;
  ledstripe.init();

  var connection = new autobahn.Connection({
    url: 'ws://localhost:8080/data',
    realm: 'ws2811'
  });

  connection.onopen = function (session) {
    function onevent(args) {
      ledstripe.update_canvas(args[0]);
    }
    session.subscribe('frame', onevent);
  };

  connection.open();
}
