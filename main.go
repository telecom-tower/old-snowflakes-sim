package main

//go:generate esc -o html.go -prefix html html

import (
	"flag"
	"net/http"

	"math/rand"

	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/vharitonsky/iniflags"
	"gitlab.com/geomyidae/ws2811sim"
	"gitlab.com/telecom-tower/pixtrix"
	"gopkg.in/jcelliott/turnpike.v2"
)

const (
	rows    = 8
	columns = 128
)

type flake struct {
	pos    int
	height int
	delay  int
	color  uint32
}

func main() {
	var debug = flag.Bool("debug", false, "be verbose")
	iniflags.Parse()

	if *debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}

	log.Infoln("Starting snowing tower")

	// create WAMP server and client
	server := turnpike.NewBasicWebsocketServer("ws2811")
	client, err := server.GetLocalClient("ws2811", nil)
	if err != nil {
		log.Panic(err)
	}

	// create ws2811 simulator
	opt := ws2811sim.DefaultOptions
	opt.WampClient = client
	opt.LedCount = rows * columns
	opt.Frequency = 800000
	ws := ws2811sim.MakeWS2811(&opt)

	frames := make([]*pixtrix.Pixtrix, 3)

	for i := range frames {
		frames[i] = &pixtrix.Pixtrix{
			Rows:   rows,
			Bitmap: make([]uint32, rows*columns),
		}
	}

	frameNr := make(chan int)
	flakes := make([]flake, columns)

	go func() {
		i := 0
		for {
			// generate k flakes
			for k := 0; k < 2; k++ {
				x := rand.Intn(columns)
				if flakes[x].height == 0 {
					flakes[x].height = 2 + rand.Intn(2)
					flakes[x].pos = 0
					flakes[x].delay = 2 + rand.Intn(4)
					rg := 255 - 32 + uint32(rand.Intn(32))
					flakes[x].color = rg<<16 + rg<<8 + rg
				}
			}

			// Clear

			for x := 0; x < columns; x++ {
				for y := 0; y < rows; y++ {
					if y <= flakes[x].pos && y > flakes[x].pos-flakes[x].height {
						frames[i].SetPixel(x, y, flakes[x].color)
					} else {
						frames[i].SetPixel(x, y, 0x110033)
					}
				}
				if flakes[x].height > 0 {
					flakes[x].pos++
					if flakes[x].pos-flakes[x].height-flakes[x].delay > rows {
						flakes[x].pos = 0
						flakes[x].height = 0
						flakes[x].delay = 0
					}
				}
			}
			frameNr <- i
			i = (i + 1) % 3
		}
	}()

	for x := 0; x < columns; x++ {
		frames[0].SetPixel(x, 2, 0xffffff)
	}

	go func() {
		for {
			i := <-frameNr
			ws.SetBitmap(frames[i].InterleavedStripes()[0])
			ws.Render()
			ws.Wait()
			time.Sleep(50 * time.Millisecond)
		}
	}()

	http.Handle("/data", server)
	http.Handle("/", http.FileServer(FS(false)))
	log.Println("Serving at localhost:8080...")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
